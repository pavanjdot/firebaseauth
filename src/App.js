import React from 'react';
import './App.css';
import  logo from './logo.svg';



class App extends React.Component{
    constructor(props){
        super(props);

        this.state = {userInput: ''};


        this.handleUserInput = this.handleUserInput.bind(this);
    }

    handleUserInput(e){
        this.setState({userInput : e.target.value})
    }
    render(){
        return(
            <div>
                <div className='logoh'>
                    <a href='https://getstarted.in/'> <img src={logo} className='logo'/></a>
                    <h3>Hello, Welcome to Live class from Getstarted.</h3>

                    <input type='text' onChange={this.handleUserInput}/>

                    <h1>Controlled Input </h1>
                    <p>{this.state.userInput}</p>
                </div>



            </div>
        );
    }
}

export default App;
