import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import Signup from './signup';
import Signin from './signin';
import { Route, Link, BrowserRouter as Router } from 'react-router-dom';
import GoogleSignup from "./GoogleSignup";



const routing = (
    <Router>
        <div>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/signup">SignUp</Link>
                </li>
                <li>
                    <Link to="/signin">SignIn</Link>
                </li>
                <li>
                    <Link to="/GoogleSignup">GoogleSignup</Link>
                </li>
            </ul>
            <Route path="/" component={App} />
            <Route path="/signup" component={Signup} />
            <Route path="/signin" component={Signin} />
            <Route path="/GoogleSignup" component={GoogleSignup} />
        </div>
    </Router>
);
ReactDOM.render(routing,  document.getElementById('root'));